class ReverseProxyError(Exception):
    def __init__(self, code, msg, headers):
        super(ReverseProxyError, self).__init__(msg)
        self.code = code
        self.msg = msg
        self.headers = headers


class NotFoundError(ReverseProxyError):
    pass


class BadRequestError(ReverseProxyError):
    pass
