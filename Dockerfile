FROM python:3.9-alpine AS builder

ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_EXTRA_INDEX_URL=https://pip.grassrootseconomics.net

WORKDIR /home/root/app

RUN apk add --update build-base linux-headers libffi-dev openssl-dev

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
ADD requirements.txt .

RUN --mount=type=cache,target=/root/.cache \
    pip install -r requirements.txt

FROM python:3.9-alpine
WORKDIR /home/root/app

RUN apk add --no-cache gnupg

COPY . .
COPY ./.well-known /var/lib/cic-auth-helper/pgp

COPY --from=builder /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

EXPOSE 80
CMD uwsgi --wsgi-file cic_auth_helper/runnable/server.py --http :80 --pyargv "-vv --forward-to http://cic-meta-server:80"